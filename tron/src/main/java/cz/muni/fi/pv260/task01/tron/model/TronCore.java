package cz.muni.fi.pv260.task01.tron.model;

import cz.muni.fi.pv260.task01.engine.Core;
import cz.muni.fi.pv260.task01.tron.controls.TronControlsManager;

import java.awt.Color;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.List;

/**
 * Tron game.
 */
public class TronCore extends Core {

    // PRIVATE ATTRIBUTES

    private TronControlsManager controlsManager;

    // CONSTRUCTORS

    public TronCore(int width, int height, TronControlsManager controlsManager) {
        super(width, height);
        this.controlsManager = controlsManager;
        initializeBikes();
    }

    // OVERRIDEN METHODS

    @Override
    public void update() {
        moveBikes();
        checkForCollisions();
    }

    // PRIVATE HELPER METHODS

    private void checkForCollisions() {
        for (Bike bike : getBikes()) {
            if (checkHasCollided(bike)) {
                stop();
            }
        }
    }

    private boolean checkHasCollided(Bike bike) {
        return getBikes().stream().anyMatch(b -> b.hasCollided(bike));
    }

    private void initializeBikes() {
        Bike greenBike =  new Bike(40, 40, 5, Direction.RIGHT, Color.GREEN);
        Bike redBike = new Bike(240, 360, 5, Direction.RIGHT, Color.RED);

        controlsManager
            .registerKeySchema(greenBike, KeyEvent.VK_UP, KeyEvent.VK_DOWN, KeyEvent.VK_LEFT, KeyEvent.VK_RIGHT);

        controlsManager.registerMouseSchema(redBike, MouseEvent.BUTTON1, MouseEvent.BUTTON3);

        addActor(greenBike);
        addActor(redBike);
    }

    private void moveBikes() {
        getBikes().stream().forEach(b -> b.move(width, height));
    }

    // GETTERS

    public List<Bike> getBikes() {
        return getActorsOfType(Bike.class);
    }
}