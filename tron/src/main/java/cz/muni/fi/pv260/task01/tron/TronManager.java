package cz.muni.fi.pv260.task01.tron;

import cz.muni.fi.pv260.task01.engine.GameManager;
import cz.muni.fi.pv260.task01.tron.controls.TronControlsManager;
import cz.muni.fi.pv260.task01.tron.model.TronCore;
import cz.muni.fi.pv260.task01.tron.renderer.TronRenderer;

import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

/**
 * Tron game manager.
 */
public class TronManager extends GameManager {

    // CONSTRUCTORS

    public TronManager() {
        super();
        TronControlsManager controlsManager = new TronControlsManager();
        TronCore core = new TronCore(getScreenManager().getWidth(), getScreenManager().getHeight(), controlsManager);

        this.controlsManager = controlsManager;
        this.core = core;
        this.renderer = new TronRenderer(core);
    }
}
