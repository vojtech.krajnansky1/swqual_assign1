package cz.muni.fi.pv260.task01.tron.controls;

import cz.muni.fi.pv260.task01.tron.model.Bike;
import cz.muni.fi.pv260.task01.tron.model.Direction;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;

/**
 * Handler for Bike key controls.
 */
public class KeyControlsManager implements KeyListener {

    // CLASS ATTRIBUTES

    private List<KeySchema> keySchemas;

    // CONSTRUCTORS

    public KeyControlsManager() {
        this.keySchemas = new ArrayList<>();
    }

    // PUBLIC METHODS

    /**
     * Register a new Bike key control schema.
     *
     * @param bike The bike to register the schema for.
     * @param up The key code for the up direction.
     * @param down The key code for the down direction.
     * @param left The key code for the left direction.
     * @param right The key code for the right direction.
     */
    public void registerSchema(Bike bike, int up, int down, int left, int right) {
        keySchemas.add(new KeySchema(bike, up, down, left, right));
    }

    // OVERRIDEN METHODS

    @Override
    public void keyReleased(KeyEvent event) {

    }

    @Override
    public void keyPressed(KeyEvent event) {
        for (KeySchema schema : keySchemas) {
            Direction newDirection = schema.getDirection(event.getKeyCode());

            if (newDirection != null) {
                Bike bike = schema.getBike();

                if (bike.getDirection().getOpposite() != newDirection) {
                    bike.setDirection(newDirection);
                }
            }
        }
    }

    @Override
    public void keyTyped(KeyEvent event) {

    }
}