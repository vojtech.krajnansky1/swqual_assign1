package cz.muni.fi.pv260.task01.tron;

/**
 * Entry point for the TRON game.
 */
public class Main {

    public static void main(String[] args) {
        new TronManager().run();
    }
}
