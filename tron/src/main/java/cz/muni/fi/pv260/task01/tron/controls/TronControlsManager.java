package cz.muni.fi.pv260.task01.tron.controls;

import cz.muni.fi.pv260.task01.engine.ControlsManager;
import cz.muni.fi.pv260.task01.tron.model.Bike;

import java.awt.Window;

/**
 * The controls manager for Tron.
 */
public class TronControlsManager implements ControlsManager {

    // CLASS ATTRIBUTES

    private KeyControlsManager keyControlsManager;
    private MouseControlsManager mouseControlsManager;

    // CONSTRUCTORS

    public TronControlsManager() {
        this.keyControlsManager = new KeyControlsManager();
        this.mouseControlsManager = new MouseControlsManager();
    }

    // PUBLIC METHODS

    /**
     * Register a new Bike key control schema.
     *
     * @param bike The bike to register the schema for.
     * @param up The key code for the up direction.
     * @param down The key code for the down direction.
     * @param left The key code for the left direction.
     * @param right The key code for the right direction.
     */
    public void registerKeySchema(Bike bike, int up, int down, int left, int right) {
        keyControlsManager.registerSchema(bike, up, down, left, right);
    }

    /**
     * Register a new Bike mouse control schema.
     *
     * @param bike The bike to register the schema for.
     * @param turnLeft The key code to change the direction counter-clockwise.
     * @param turnRight The key code to change the direction clockwise.
     */
    public void registerMouseSchema(Bike bike, int turnLeft, int turnRight) {
        mouseControlsManager.registerSchema(bike, turnLeft, turnRight);
    }

    // OVERRIDEN METHODS

    @Override
    public void registerControls(Window window) {
        window.addKeyListener(keyControlsManager);
        window.addMouseListener(mouseControlsManager);
    }
}
