package cz.muni.fi.pv260.task01.tron.renderer;

import cz.muni.fi.pv260.task01.engine.GameRenderer;
import cz.muni.fi.pv260.task01.tron.model.Bike;
import cz.muni.fi.pv260.task01.tron.model.TronCore;

import java.awt.Color;
import java.awt.Graphics2D;

/**
 * Tron game renderer.
 */
public class TronRenderer implements GameRenderer {

    // CLASS ATTRIBUTES

    TronCore core;

    // CONSTRUCTORS

    public TronRenderer(TronCore core) {
        this.core = core;
    }

    // OVERRIDEN METHODS

    @Override
    public void draw(Graphics2D graphics) {
        drawBackground(graphics);
        drawPaths(graphics);
    }

    // PRIVATE HELPER METHODS

    private void drawBackground(Graphics2D graphics) {
        graphics.setColor(Color.BLACK);
        graphics.fillRect(0, 0, core.getWidth(), core.getHeight());
    }

    private void drawBikePath(Graphics2D graphic, Bike bike) {
        graphic.setColor(bike.getColor());
        bike.getPath().stream().forEach(coord -> graphic.fillRect((int) coord.getX(), (int) coord.getY(), 10, 10));
    }

    private void drawPaths(Graphics2D graphic) {
        core.getBikes().stream().forEach(b -> drawBikePath(graphic, b));
    }
}
