package cz.muni.fi.pv260.task01.tron.controls;

import cz.muni.fi.pv260.task01.tron.model.Bike;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;

/**
 * Handler for Bike key controls.
 */
public class MouseControlsManager implements MouseListener {

    // CLASS ATTRIBUTES

    private List<MouseSchema> mouseSchemas;

    // CONSTRUCTORS

    public MouseControlsManager() {
        this.mouseSchemas = new ArrayList<>();
    }

    // PUBLIC METHODS

    /**
     * Register a new Bike mouse control schema.
     *
     * @param bike The bike to register the schema for.
     * @param turnLeft The key code to change the direction counter-clockwise.
     * @param turnRight The key code to change the direction clockwise.
     */
    public void registerSchema(Bike bike, int turnLeft, int turnRight) {
        mouseSchemas.add(new MouseSchema(bike, turnLeft, turnRight));
    }

    // OVERRIDEN METHODS

    @Override
    public void mouseClicked(MouseEvent event) {

    }

    @Override
    public void mousePressed(MouseEvent event) {
        for (MouseSchema schema : mouseSchemas) {
            Bike bike = schema.getBike();

            if (event.getButton() == schema.getTurnLeft()) {
                bike.setDirection(bike.getDirection().getCounterClockwise());
            } else if (event.getButton() == schema.getTurnRight()) {
                bike.setDirection(bike.getDirection().getClockwise());
            }
        }
    }

    @Override
    public void mouseExited(MouseEvent event) {

    }

    @Override
    public void mouseEntered(MouseEvent event) {

    }

    @Override
    public void mouseReleased(MouseEvent event) {

    }
}
