package cz.muni.fi.pv260.task01.tron.model;

import cz.muni.fi.pv260.task01.engine.Actor;

import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * A player TRON bike.
 */
public class Bike extends Actor {

    // CLASS ATTRIBUTES

    private final Color color;

    private Direction direction;
    private List<Point> path = new ArrayList<>();
    private int speed;

    // CONSTRUCTORS

    public Bike(int posX, int posY, int speed, Direction direction, Color color) {
        super(posX, posY);
        this.direction = direction;
        this.color = color;
        this.speed = speed;
    }

    // PUBLIC METHODS

    /**
     * Move bike in its current direction.
     *
     * @param maxWidth The maximum width of the playing field.
     * @param maxHeight The maximum height of the playing field.
     */
    public void move(int maxWidth, int maxHeight) {
        updateCurrentPosition(maxWidth, maxHeight);
        path.add(new Point((int) getPosX(),(int) getPosY()));
    }

    /**
     * Check whether the bike has collided with another bike.
     *
     * @param other The bike for which to check whether this bike has collided with it.
     * @return true if the bike has collided with the other bike, false otherwise.
     */
    public boolean hasCollided(Bike other) {
        // Ignore latest coordinate as that is the current position.
        for (int pos = 0; pos < getPath().size() - 1; pos++) {
            Point otherCoordinate = other.getPath().get(pos);
            if (getPosition().equals(otherCoordinate)) {
                return true;
            }
        }
        return false;
    }

    // PRIVATE HELPER METHODS

    private void moveDown(int maxHeight) {
        if (getPosY() < maxHeight) {
            setPosY(getPosY() + speed);
        } else {
            setPosY(0);
        }
    }

    private void moveLeft(int maxWidth) {
        if (getPosX() > 0) {
            setPosX(getPosX() - speed);
        } else {
            setPosX(maxWidth);
        }
    }

    private void moveRight(int maxWidth) {
        if (getPosX() < maxWidth) {
            setPosX(getPosX() + speed);
        } else {
            setPosX(0);
        }
    }

    private void moveUp(int maxHeight) {
        if (getPosY() > 0) {
            setPosY(getPosY() - speed);
        } else {
            setPosY(maxHeight);
        }
    }

    private void updateCurrentPosition(int maxWidth, int maxHeight) {
        switch (getDirection()) {
            case UP:
                moveUp(maxHeight);
                break;

            case RIGHT:
                moveRight(maxWidth);
                break;

            case DOWN:
                moveDown(maxHeight);
                break;

            case LEFT:
                moveLeft(maxWidth);
                break;

            default:
                break;
        }
    }

    // GETTERS AND SETTERS

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public List<Point> getPath() {
        return Collections.unmodifiableList(path);
    }

    public Color getColor() {
        return color;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }
}
