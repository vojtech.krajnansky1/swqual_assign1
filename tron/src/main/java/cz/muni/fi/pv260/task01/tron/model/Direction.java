package cz.muni.fi.pv260.task01.tron.model;

import java.util.HashMap;
import java.util.Map;

public enum Direction {

    // ENUM DEFINITION

    UP(0, 1, 3, 2),
    DOWN(1, 0, 2, 3),
    LEFT(2, 3, 0, 1),
    RIGHT(3, 2, 1, 0);

    // CLASS ATTRIBUTES

    // Lookup table to obtain Direction from its value.
    private static final Map<Integer, Direction> valueToDirection = new HashMap<>();

    static {
        for (Direction direction : Direction.values()) {
            valueToDirection.put(direction.getValue(), direction);
        }
    }

    private int value;
    private int opposite;
    private int clockwise;
    private int counterClockwise;

    // CONSTRUCTORS

    private Direction(int value, int opposite, int clockwise, int counterClockwise) {
        this.value = value;
        this.opposite = opposite;
        this.clockwise = clockwise;
        this.counterClockwise = counterClockwise;
    }

    // GETTERS

    public int getValue() {
        return value;
    }

    public Direction getOpposite() {
        return valueToDirection.get(opposite);
    }

    public Direction getClockwise() {
        return valueToDirection.get(clockwise);
    }

    public Direction getCounterClockwise() {
        return valueToDirection.get(counterClockwise);
    }
}