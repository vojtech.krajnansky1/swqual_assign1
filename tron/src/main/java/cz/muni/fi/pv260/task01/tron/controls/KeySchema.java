package cz.muni.fi.pv260.task01.tron.controls;

import cz.muni.fi.pv260.task01.tron.model.Bike;
import cz.muni.fi.pv260.task01.tron.model.Direction;

/**
 * Keyboard schema for Bike right, up, down, left movement.
 */
public class KeySchema {

    // CLASS ATTRIBUTES

    private final Bike bike;

    private final int up;
    private final int down;
    private final int left;
    private final int right;

    // CONSTRUCTORS

    public KeySchema(Bike bike, int up, int down, int left, int right) {
        this.bike = bike;
        this.up = up;
        this.down = down;
        this.left = left;
        this.right = right;
    }

    // PUBLIC METHODS

    /**
     * Gets the Direction corresponging to a given key code.
     *
     * @param key The key code.
     * @return The Direction corresponding to the given key code if it has been registered, null otherwise.
     */
    public Direction getDirection(int key) {
        if (key == getUp()) {
            return Direction.UP;
        } else if (key == getDown()) {
            return Direction.DOWN;
        } else if (key == getLeft()) {
            return Direction.LEFT;
        } else if (key == getRight()) {
            return Direction.RIGHT;
        }

        return null;
    }

    // GETTERS

    public Bike getBike() {
        return bike;
    }

    public int getUp() {
        return up;
    }

    public int getDown() {
        return down;
    }

    public int getLeft() {
        return left;
    }

    public int getRight() {
        return right;
    }
}