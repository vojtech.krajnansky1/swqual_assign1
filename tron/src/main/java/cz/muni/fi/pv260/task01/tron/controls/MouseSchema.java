package cz.muni.fi.pv260.task01.tron.controls;

import cz.muni.fi.pv260.task01.tron.model.Bike;

/**
 * Mouse schema for left/right movement direction change.
 */
public class MouseSchema {

    // CLASS ATTRIBUTES

    private final Bike bike;

    private final int turnLeft;
    private final int turnRight;

    // CONSTRUCTORS

    public MouseSchema(Bike bike, int turnLeft, int turnRight) {
        this.bike = bike;
        this.turnLeft = turnLeft;
        this.turnRight = turnRight;
    }

    // GETTERS

    public Bike getBike() {
        return bike;
    }

    public int getTurnLeft() {
        return turnLeft;
    }

    public int getTurnRight() {
        return turnRight;
    }
}
