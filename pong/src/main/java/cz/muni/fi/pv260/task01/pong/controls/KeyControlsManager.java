package cz.muni.fi.pv260.task01.pong.controls;

import cz.muni.fi.pv260.task01.pong.model.Pad;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;

public class KeyControlsManager implements KeyListener {

    // CLASS ATTRIBUTES

    private List<KeySchema> keySchemas;

    // CONSTRUCTORS

    public KeyControlsManager() {
        this.keySchemas = new ArrayList<>();
    }

    // PUBLIC METHODS

    /**
     * Register new Pad key controls.
     *
     * @param pad The pad for which to register the schema.
     * @param up The key code to make the pad move up.
     * @param down The key code to make the pad move down.
     */
    public void registerSchema(Pad pad, int up, int down) {
        keySchemas.add(new KeySchema(pad, up, down));
    }

    // OVERRIDEN METHODS

    @Override
    public void keyPressed(KeyEvent event) {
        for (KeySchema schema : keySchemas) {
            if (event.getKeyCode() == schema.getUp()) {
                schema.getPad().setMovingUp();
            } else if (event.getKeyCode() == schema.getDown()) {
                schema.getPad().setMovingDown();
            }
        }
    }

    @Override
    public void keyReleased(KeyEvent event) {
        for (KeySchema schema : keySchemas) {
            if (event.getKeyCode() == schema.getUp() || event.getKeyCode() == schema.getDown()) {
                schema.getPad().setNotMoving();
            }
        }
    }

    @Override
    public void keyTyped(KeyEvent event) {

    }
}