package cz.muni.fi.pv260.task01.pong.controls;

import cz.muni.fi.pv260.task01.pong.model.Pad;

/**
 * Key controls schema for moving up and down.
 */
public class KeySchema {

    // CLASS ATTRIBUTES

    private final Pad pad;

    private final int up;
    private final int down;

    // CONSTRUCTORS

    public KeySchema(Pad pad, int up, int down) {
        this.pad = pad;
        this.up = up;
        this.down = down;
    }

    // GETTERS

    public Pad getPad() {
        return pad;
    }

    public int getUp() {
        return up;
    }

    public int getDown() {
        return down;
    }
}