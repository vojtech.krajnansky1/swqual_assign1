package cz.muni.fi.pv260.task01.pong.model;

import cz.muni.fi.pv260.task01.engine.Core;
import cz.muni.fi.pv260.task01.pong.controls.PongControlsManager;

import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.List;

/**
 * Pong game.
 */
public class PongCore extends Core {

    private PongControlsManager controlsManager;

    // CONSTRUCTORS

    public PongCore(int width, int height, PongControlsManager controlsManager) {
        super(width, height);
        this.controlsManager = controlsManager;
        initializePads();
        initializeBall();
    }

    // OVERRIDEN METHODS

    @Override
    public void update() {
        movePads();
        moveBall();
        checkForPadHits();
        checkGameOver();
    }

    // PRIVATE HELPER METHODS

    private void checkForPadHits() {
        Ball ball = getBall();
        for (Pad pad : getPads()) {
            if (ball.hasCollided(pad)) {
                ball.setVelocityRight(-ball.getVelocityRight());
            }
        }
    }

    private void checkGameOver() {
        Ball ball = getBall();
        if (ball.getPosX() < 0 || ball.getPosX() > getWidth()) {
            stop();
        }
    }

    private void moveBall() {
        getBall().move(0, getHeight());
    }

    private void movePads() {
        getPads().stream().forEach(p -> p.move(0, getHeight()));
    }

    private void initializeBall() {
	      addActor(new Ball(getWidth() / 2, getHeight() / 2, 15, 15));
	  }

    private void initializePads() {
        Pad padRight = new Pad(getWidth() - 10, getHeight() / 2, 200);
        Pad padLeft = new Pad(10, getHeight() / 2, 200);

        controlsManager.registerKeySchema(padRight, KeyEvent.VK_UP, KeyEvent.VK_DOWN);
        controlsManager.registerKeySchema(padLeft, KeyEvent.VK_W, KeyEvent.VK_S);

        addActor(padRight);
        addActor(padLeft);
    }

    // GETTERS

    public List<Pad> getPads() {
        return getActorsOfType(Pad.class);
    }

    public Ball getBall() {
        return getActorsOfType(Ball.class).get(0);
    }
}