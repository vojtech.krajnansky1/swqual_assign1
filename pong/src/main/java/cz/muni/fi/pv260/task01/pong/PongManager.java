package cz.muni.fi.pv260.task01.pong;

import cz.muni.fi.pv260.task01.engine.GameManager;
import cz.muni.fi.pv260.task01.pong.controls.PongControlsManager;
import cz.muni.fi.pv260.task01.pong.model.PongCore;
import cz.muni.fi.pv260.task01.pong.renderer.PongRenderer;

/**
 * Pong game manager.
 */
public class PongManager extends GameManager {

    // CONSTRUCTORS

    public PongManager() {
        super();
        PongControlsManager controlsManager = new PongControlsManager();
        PongCore core = new PongCore(getScreenManager().getWidth(), getScreenManager().getHeight(), controlsManager);

        this.controlsManager = controlsManager;
        this.core = core;
        this.renderer = new PongRenderer(core);
    }
}