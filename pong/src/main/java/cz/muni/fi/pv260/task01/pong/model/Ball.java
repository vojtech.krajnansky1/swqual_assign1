package cz.muni.fi.pv260.task01.pong.model;

import cz.muni.fi.pv260.task01.engine.Actor;

/**
 * Represents a Pong ball.
 */
public class Ball extends Actor {

    // CLASS ATTRIBUTES

    private int velocityDown;
    private int velocityRight;

	  // CONSTRUCTORS

	  public Ball(int posX, int posY, int velocityDown, int velocityRight) {
	      super(posX, posY);
	      this.velocityDown = velocityDown;
	      this.velocityRight = velocityRight;
	  }

    // PUBLIC METHODS

    /**
     * Checks whether the ball has collided with a pad.
     *
     * @param pad The pad to check for collision.
     * @return true if the ball has collided with the pad, false otherwise.
     */
    public boolean hasCollided(Pad pad) {
        if (!hasSameX(pad)) {
            return false;
        }

        for (int pos = pad.getLowerEnd() - 10; pos <= pad.getUpperEnd() + 10; pos++) {
            if (getPosY() == pos) {
                return true;
            }
        }

        return false;
    }

    /**
     * Moves the ball in its current direction.
     *
     * @param maxHeight The maximum height of the playing field.
     * @param minHeight The minimum height of the playing field.
     */
    public void move(int maxHeight, int minHeight) {
        setPosX(getPosX() + velocityRight);
        setPosY(getPosY() + velocityDown);
        if (wasWallHit(maxHeight, minHeight)) {
            velocityDown = -velocityDown;
        }
    }

    // PRIVATE HELPER METHODS

    private boolean hasSameX(Pad pad) {
        return getPosX() >= pad.getPosX() && getPosX() <= pad.getPosX() + 10;
    }

    private boolean wasWallHit(int maxHeight, int minHeight) {
        return getPosY() <= maxHeight || getPosY() >= minHeight - 10;
    }

    // GETTERS AND SETTERS

    public int getVelocityDown() {
        return velocityDown;
    }

    public void setVelocityDown(int velocity) {
        velocityDown = velocity;
    }

    public int getVelocityRight() {
        return velocityRight;
    }

    public void setVelocityRight(int velocity) {
        velocityRight = velocity;
    }
}