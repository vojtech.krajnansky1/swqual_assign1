package cz.muni.fi.pv260.task01.pong.controls;

import cz.muni.fi.pv260.task01.engine.ControlsManager;
import cz.muni.fi.pv260.task01.pong.model.Pad;

import java.awt.Window;

/**
 * The controls manager for the Pong game.
 */
public class PongControlsManager implements ControlsManager {

    // CLASS ATTRIBUTES

    private KeyControlsManager keyControlsManager;

    // CONSTRUCTORS

    public PongControlsManager() {
        this.keyControlsManager = new KeyControlsManager();
    }

    // PUBLIC METHODS

    /**
     * Register a new Pad key control schema.
     *
     * @param pad The pad for which to register the schema.
     * @param up The key code for moving the pad up.
     * @param down The key code for moving the pad down.
     */
    public void registerKeySchema(Pad pad, int up, int down) {
        keyControlsManager.registerSchema(pad, up, down);
    }

    // OVERRIDEN METHODS

    @Override
    public void registerControls(Window window) {
        window.addKeyListener(keyControlsManager);
    }
}