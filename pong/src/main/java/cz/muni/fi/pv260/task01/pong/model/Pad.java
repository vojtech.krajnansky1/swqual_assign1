package cz.muni.fi.pv260.task01.pong.model;

import cz.muni.fi.pv260.task01.engine.Actor;

/**
 * Represents a player's pad.
 */
public class Pad extends Actor {

    // CLASS ATTRIBUTES

    private Direction direction;
    private int size;

	  // CONSTRUCTORS

	  public Pad(int posX, int posY, int size) {
	      super(posX, posY);
	      this.size = size;
	      this.direction = Direction.NONE;
	  }

	  // PUBLIC METHODS

    /**
     * Move the pad according to its current direction.
     *
     * @param upperBoundary The uppermost boundary to which the pad can move.
     * @param lowerBoundary The lowermost boundary to which the pad can move.
     */
    public void move(int upperBoundary, int lowerBoundary) {
        if (direction == Direction.UP && getLowerEnd() >= upperBoundary) {
            setPosY(getPosY() - 15);
        } else if (direction == Direction.DOWN && getUpperEnd() <= lowerBoundary) {
            setPosY(getPosY() + 15);
        }
    }

    public void setMovingUp() {
        direction = Direction.UP;
    }

    public void setMovingDown() {
        direction = Direction.DOWN;
    }

    public void setNotMoving() {
        direction = Direction.NONE;
    }

    // GETTERS AND SETTERS

    public int getUpperEnd() {
        return (int) getPosY() + size / 2;
    }

    public int getLowerEnd() {
        return (int) getPosY() - size / 2;
    }

    // NESTED CLASSES

    private enum Direction {
        UP, DOWN, NONE
    }
}