package cz.muni.fi.pv260.task01.pong.renderer;

import cz.muni.fi.pv260.task01.engine.GameRenderer;
import cz.muni.fi.pv260.task01.pong.model.Ball;
import cz.muni.fi.pv260.task01.pong.model.Pad;
import cz.muni.fi.pv260.task01.pong.model.PongCore;

import java.awt.Color;
import java.awt.Graphics2D;

/**
 * Pong game renderer.
 */
public class PongRenderer implements GameRenderer {

    // CLASS ATTRIBUTES

    PongCore core;

    // CONSTRUCTORS

    public PongRenderer(PongCore core) {
        this.core = core;
    }

    // OVERRIDEN METHODS

    @Override
    public void draw(Graphics2D graphics) {
        drawBackground(graphics);
        drawBall(graphics);
        drawPads(graphics);
    }

    // PRIVATE HELPER METHODS

    private void drawBackground(Graphics2D graphics) {
        graphics.setColor(Color.BLACK);
		    graphics.fillRect(0, 0, core.getWidth(), core.getHeight());
    }

    private void drawBall(Graphics2D graphic) {
        graphic.setColor(Color.WHITE);
        Ball ball = core.getBall();
        graphic.fillRect((int) ball.getPosX(), (int) ball.getPosY(), 10, 10);
    }

    private void drawPad(Graphics2D graphic, Pad pad) {
        for (int pos = pad.getLowerEnd(); pos < pad.getUpperEnd(); pos++) {
            graphic.fillRect((int) pad.getPosX(), pos, 10, 10);
        }
    }

  	private void drawPads(Graphics2D graphic) {
  	    graphic.setColor(Color.WHITE);
  	    core.getPads().stream().forEach(p -> drawPad(graphic, p));
	  }
}
