package cz.muni.fi.pv260.task01.pong;

/**
 * Entry point for the Pong game.
 */
public class Main {

    public static void main(String[] args) {
        new PongManager().run();
    }
}
