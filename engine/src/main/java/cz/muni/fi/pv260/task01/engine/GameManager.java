package cz.muni.fi.pv260.task01.engine;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Window;
import java.awt.image.BufferedImage;

/**
 * Core game manager. Takes care of running the game.
 */
public abstract class GameManager {

    // CLASS ATTRIBUTES

    private ScreenManager screenManager;
    private Window window;

    protected ControlsManager controlsManager;
    protected Core core;
    protected GameRenderer renderer;

    // CONSTRUCTORS

    public GameManager() {
        screenManager = new ScreenManager();
        initWindow();
    }

    // PUBLIC METHODS

    /**
     * Start the game.
     */
    public void run() {
        try {
            controlsManager.registerControls(window);
            gameLoop();
        } finally {
            screenManager.restoreScreen();
        }
    }

    // PRIVATE HELPER MEHTODS

    private void initWindow() {
        screenManager.setFullScreen();

        window = screenManager.getFullScreenWindow();
        window.setFont(new Font("Arial", Font.PLAIN, 20));
        window.setBackground(Color.WHITE);
        window.setForeground(Color.RED);

        window.setCursor(
                window.getToolkit()
                        .createCustomCursor(
                                new BufferedImage(3, 3, BufferedImage.TYPE_INT_ARGB),
                                new Point(0, 0),
                                "null"
                        )
        );
    }

    private void gameLoop() {
        while (true) {
            Graphics2D graphics = screenManager.getGraphics();
            core.update();
            renderer.draw(graphics);

            graphics.dispose();
            screenManager.update();
            try {
                Thread.sleep(20);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    // GETTERS

    public ScreenManager getScreenManager() {
        return screenManager;
    }
}