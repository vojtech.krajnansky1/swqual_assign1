package cz.muni.fi.pv260.task01.engine;

import java.awt.DisplayMode;
import java.awt.Graphics2D;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Window;
import java.awt.image.BufferStrategy;

import javax.swing.JFrame;

/**
 * Screen/window management handler.
 */
public class ScreenManager {

    // CLASS ATTRIBUTES

    private GraphicsDevice device;

    private static final DisplayMode[] modes = {
            //new DisplayMode(1920, 1080, 32, 0),
            new DisplayMode(1680, 1050, 32, 0),
            //new DisplayMode(1280, 1024, 32, 0),
            new DisplayMode(800, 600, 32, 0),
            new DisplayMode(800, 600, 24, 0),
            new DisplayMode(800, 600, 16, 0),
            new DisplayMode(640, 480, 32, 0),
            new DisplayMode(640, 480, 24, 0),
            new DisplayMode(640, 480, 16, 0)
    };

    // CONSTRUCTORS

    public ScreenManager() {
        device = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
    }

    // PUBLIC METHODS

    /**
     * Initialize fullscreen overlay.
     */
    public void setFullScreen() {
        JFrame frame = new JFrame();

        frame.setUndecorated(true);
        frame.setIgnoreRepaint(true);
        frame.setResizable(false);

        device.setFullScreenWindow(frame);

        DisplayMode mode = findFirstCompatibleMode();

        if (mode != null && device.isDisplayChangeSupported()) {
            try {
                device.setDisplayMode(mode);
            } catch (Exception ex) {

            }
            frame.createBufferStrategy(2);
        }
    }

    /**
     * Update window.
     */
    public void update() {
        Window window = device.getFullScreenWindow();

        if (window != null) {
            BufferStrategy strategy = window.getBufferStrategy();

            if (!strategy.contentsLost()) {
                strategy.show();
            }
        }
    }

    /**
     * Remove window.
     */
    public void restoreScreen() {
        Window window = device.getFullScreenWindow();

        if (window != null) {
            window.dispose();
        }

        device.setFullScreenWindow(null);
    }

    // PRIVATE HELPER METHODS

    private boolean displayBitDepthsMatch(DisplayMode mode1, DisplayMode mode2) {
        return mode1.getBitDepth() == DisplayMode.BIT_DEPTH_MULTI
                || mode2.getBitDepth() == DisplayMode.BIT_DEPTH_MULTI
                || mode1.getBitDepth() == mode2.getBitDepth();
    }

    private boolean displayModesMatch(DisplayMode mode1, DisplayMode mode2) {
        return displaySizesMatch(mode1, mode2)
                && displayBitDepthsMatch(mode1, mode2)
                && displayRefreshRatesMatch(mode1, mode2);
    }

    private boolean displayRefreshRatesMatch(DisplayMode mode1, DisplayMode mode2) {
        return mode1.getRefreshRate() == DisplayMode.REFRESH_RATE_UNKNOWN
                || mode2.getRefreshRate() == DisplayMode.REFRESH_RATE_UNKNOWN
                || mode1.getRefreshRate() == mode2.getRefreshRate();
    }

    private boolean displaySizesMatch(DisplayMode mode1, DisplayMode mode2) {
        return mode1.getWidth() == mode2.getWidth() && mode1.getHeight() == mode2.getHeight();
    }

    private DisplayMode findFirstCompatibleMode() {
        for (DisplayMode mode1 : modes) {
            for (DisplayMode mode2 : device.getDisplayModes()) {
                if (displayModesMatch(mode1, mode2)) {
                    return mode1;
                }
            }
        }

        return null;
    }

    // GETTERS

    public Window getFullScreenWindow() {
        return device.getFullScreenWindow();
    }

    public Graphics2D getGraphics() {
        Window window = device.getFullScreenWindow();

        if (window != null) {
            BufferStrategy strategy = window.getBufferStrategy();
            return (Graphics2D) strategy.getDrawGraphics();
        }

        return null;
    }

    public int getHeight() {
        Window window = device.getFullScreenWindow();
        return window == null ? 0 : window.getHeight();
    }

    public int getWidth() {
        Window window = device.getFullScreenWindow();
        return window == null ? 0 : window.getWidth();
    }
}