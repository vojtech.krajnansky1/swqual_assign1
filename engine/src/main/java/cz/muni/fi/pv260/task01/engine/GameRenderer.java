package cz.muni.fi.pv260.task01.engine;

import java.awt.Graphics2D;

/**
 * Core game renderer.
 */
public interface GameRenderer {

    /**
     * Draw current game state.
     *
     * @param graphics The coordinate system in which to draw.
     */
    void draw(Graphics2D graphics);
}