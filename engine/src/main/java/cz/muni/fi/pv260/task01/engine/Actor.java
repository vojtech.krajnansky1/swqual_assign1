package cz.muni.fi.pv260.task01.engine;

import java.awt.Point;

/**
 * Represents an actor in the game - can be virtually any actor - a wall, an animate object, a player character.
 */
public class Actor {

    // CLASS ATTRIBUTES

    private Point position;

    // CONSTRUCTORS

    public Actor(int posX, int posY) {
        position = new Point(posX, posY);
    }

    // GETTERS AND SETTERS

    public Point getPosition() {
        return position;
    }

    public double getPosX() {
        return position.getX();
    }

    public void setPosX(double posX) {
        position.setLocation(posX, getPosY());
    }

    public double getPosY() {
        return position.getY();
    }

    public void setPosY(double posY) {
        position.setLocation(getPosX(), posY);
    }
}