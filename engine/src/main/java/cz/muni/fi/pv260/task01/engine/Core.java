package cz.muni.fi.pv260.task01.engine;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Core game. Represents the playing field and the actors found within.
 */
public abstract class Core {

    // CLASS ATTRIBUTES

    protected List<Actor> actors;
    protected int width;
    protected int height;

    // CONSTRUCTORS

    public Core(int width, int height) {
        this.actors = new ArrayList<>();
        this.width = width;
        this.height = height;
    }

    // ABSTRACT METHODS

    /**
     * Update the game state.
     */
    public abstract void update();

    // PROTECTED METHODS

    /**
     * Add a new actor to the game.
     *
     * @param actor The actor to be added.
     */
    protected void addActor(Actor actor) {
        actors.add(actor);
    }

    /**
     * Remove an actor from the game.
     *
     * @param actor The actor to be removed.
     */
    protected void removeActor(Actor actor) {
        actors.remove(actor);
    }

    /**
     * Stop the game.
     */
    protected void stop() {
        System.exit(0);
    }

    // GETTERS AND SETTERS

    public List<Actor> getActors() {
        return Collections.unmodifiableList(actors);
    }

    /**
     * Get actors of a certain type.
     *
     * @param type The class type to retreive.
     * @return A list of actors of given type, casted to that type.
     */
    public <T> List<T> getActorsOfType(Class<T> type) {
        return Collections.unmodifiableList(
                (List<T>) actors.stream().filter(a -> type.isInstance(a)).collect(Collectors.toList())
        );
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }
}