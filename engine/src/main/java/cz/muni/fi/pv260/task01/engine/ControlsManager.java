package cz.muni.fi.pv260.task01.engine;

import java.awt.Window;

/**
 * Core game controls manager.
 */
public interface ControlsManager {

    /**
     * Register all necessary event listeners to enable game controls.
     *
     * @param window The window to which to register event listeners.
     */
    void registerControls(Window window);
}